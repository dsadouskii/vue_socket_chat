let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);

app.get('/', function(req, res){
	// res.send('<h1>Hello socket.io</h1>');
	res.sendFile(__dirname + '/index.html')
});

http.listen(3000, function(){
	console.log('Connected done')
});

io.on('connection', function(socket){
	console.log('Socket-connection');
	io.emit('connections', Object.keys(io.sockets.connected).length);
	socket.on('disconnect', function(){
		console.log('Disconnect-socket')
	});
	socket.on('Created', (data)=>{
		// io.emit('Createda', (data));
		socket.broadcast.emit('Created', (data));
	});
	socket.on('chat-message', (data)=>{
		socket.broadcast.emit('chat-message', (data));
	});
	socket.on('typing', (data)=>{
		socket.broadcast.emit('typing', (data));
	});
	socket.on('stopTyping', (data)=>{
		socket.broadcast.emit('stopTyping', (data));
	});
	socket.on('joined', (data)=>{
		socket.broadcast.emit('joined', (data));
	});
	socket.on('leaved', (data)=>{
		socket.broadcast.emit('leaved', (data));
	});
});